import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { MenuComponent } from './shared/components/menu/menu.component';
import { ProgramacionComponent } from './modules/programacion/programacion.component';
import { EntregaComponent } from './modules/entrega/entrega.component';
import { ReporteEntregaComponent } from './modules/reporte-entrega/reporte-entrega.component';

import {RoutingModule} from './routing/routing.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    ProgramacionComponent,
    EntregaComponent,
    ReporteEntregaComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
