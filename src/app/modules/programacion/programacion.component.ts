import { Component, OnInit } from '@angular/core';
declare var require: any;
var tblProgramacion_js : any;
declare var $:any;
var alertify  = require("alertifyjs");

@Component({
  selector: 'app-programacion',
  templateUrl: './programacion.component.html',
  styleUrls: ['./programacion.component.css']
})
export class ProgramacionComponent implements OnInit {

  public programaciones = [];
  constructor() {
    this.programaciones = [
      {estado: "no programada", noPrescripcion : "2019365554771", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-06-10", cantidad:"20", codser:"1002", nombre:"Aceta caja por 20 tabletas", tipo: "medicamento"},
       {estado: "programada", noPrescripcion : "201933388877", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-07-10", cantidad:"20", codser:"1002", nombre:"Prega caja por 20 tabletas", tipo: "medicamento"},
       {estado: "no programada", noPrescripcion : "201978978979", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-06-11", cantidad:"20", codser:"1002", nombre:"Dolex caja por 20 tabletas", tipo: "medicamento"},
       {estado: "programada", noPrescripcion : "201978797897", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-06-12", cantidad:"20", codser:"1002", nombre:"Aceta caja por 20 tabletas", tipo: "medicamento"},
       {estado: "no programada", noPrescripcion : "201936393663", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-06-10", cantidad:"20", codser:"1002", nombre:"Aceta caja por 20 tabletas", tipo: "medicamento"},
       {estado: "no programada", noPrescripcion : "201936393663", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-06-10", cantidad:"20", codser:"1002", nombre:"Aceta caja por 20 tabletas", tipo: "medicamento"},
       {estado: "no programada", noPrescripcion : "201936393663", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-06-15", cantidad:"20", codser:"1002", nombre:"Aceta caja por 20 tabletas", tipo: "medicamento"},
       {estado: "programada", noPrescripcion : "201936393663", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-06-10", cantidad:"20", codser:"1002", nombre:"Dolex caja por 20 tabletas", tipo: "medicamento"},
       {estado: "no programada", noPrescripcion : "201936393663", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-05-31", cantidad:"20", codser:"1002", nombre:"Aceta caja por 20 tabletas", tipo: "medicamento"},
       {estado: "no programada", noPrescripcion : "201936393663", tipoDoc:"CC", documento: "13772342",
       municipio: "Propayan", fechamaxent: "2019-06-10", cantidad:"20", codser:"1002", nombre:"Dolex caja por 20 tabletas", tipo: "medicamento"},
       {estado: "programada", noPrescripcion : "201936393663", tipoDoc:"CC", documento: "13772342",
       municipio: "Popayan", fechamaxent: "2019-08-11", cantidad:"20", codser:"1002", nombre:"Dolex caja por 20 tabletas", tipo: "medicamento"}
      
    ];
   }

  ngOnInit() {
    
  }

  ngAfterViewInit(){
    
  }

  buscar(){
    let self = this;
    if($("#fechaProgramacion").val()=="" && $("#noPrescProgramacion").val()==""){
      alertify.error("Debe Ingresar algun tipo de busqueda");
    }else{
      $("#modalCargando").modal("show");
      setTimeout(function(){
        $("#modalCargando").modal("hide");
        self.refrescarTabla();
      }, 3000);
    }
  }

  refrescarTabla() {
    let self =  this;
    if (tblProgramacion_js) {
      tblProgramacion_js.destroy();
    }
    setTimeout(function () {
      tblProgramacion_js = $('#tblProgramacion').DataTable({
            ordering: false,
            destroy: true,
            dom: '<lf<t>ip>',
            scrollY: "400px",
            scrollX: true,
            data: self.programaciones,
            columns:[
              { data: "programar", defaultContent:'<button class="btn btn-success">Programar</button>'},
              { data: "anular", defaultContent:'<button class="btn btn-danger">Anular</button>'},
              { data: "estado"},
              { data: "noPrescripcion"},    
              { data: "tipoDoc"},
              { data: "documento"},
              { data: "municipio"},
              { data: "fechamaxent"},
              { data: "cantidad"},
              { data: "codser"},
              { data: "nombre"},
              { data: "tipo"}  
            ],
            language: {
                url: '../../../assets/plugins/datatables/spanish.json'
            },
            buttons: [
                { extend: 'pdf', },
                { extend: 'csv', },
                { extend: 'excel', }
            ],
        });
    }, 0.1);
}

}
