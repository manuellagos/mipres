import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {ProgramacionComponent} from '../modules/programacion/programacion.component';
import {EntregaComponent} from '../modules/entrega/entrega.component';
import {ReporteEntregaComponent} from '../modules/reporte-entrega/reporte-entrega.component';

const routes : Routes = [
  {
    path : 'programacion',
    component : ProgramacionComponent
  },
  {
    path : 'entrega',
    component : EntregaComponent
  },
  {
    path : 'reporte-entrega',
    component : ReporteEntregaComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
